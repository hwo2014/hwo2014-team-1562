import unittest
from speedlimits import SpeedLimits, MAX_SPEED, MIN_SPEED


class TestSpeedLimits(unittest.TestCase):
    def setUp(self):
        self.limiter = SpeedLimits()

    def test_updating_lower_limit(self):
        radius = 150
        self.assertEqual(MIN_SPEED, self.limiter.get_lower_limit(radius))
        self.limiter.update_lower_limit(radius, 5)
        self.assertEqual(5, self.limiter.get_lower_limit(radius))
        self.limiter.update_lower_limit(radius, 4)
        self.assertEqual(5, self.limiter.get_lower_limit(radius))
        self.limiter.update_lower_limit(radius, 7)
        self.assertEqual(7, self.limiter.get_lower_limit(radius))

    def test_updating_upper_limit(self):
        radius = 150
        self.assertEqual(MAX_SPEED, self.limiter.get_upper_limit(radius))
        self.limiter.update_upper_limit(radius, 5)
        self.assertEqual(5, self.limiter.get_upper_limit(radius))
        self.limiter.update_upper_limit(radius, 4)
        self.assertEqual(4, self.limiter.get_upper_limit(radius))
        self.limiter.update_upper_limit(radius, 7)
        self.assertEqual(4, self.limiter.get_upper_limit(radius))

    def test_updating_lower_limit_with_existing_upper_limit(self):
        radius = 150
        self.limiter.update_upper_limit(radius, 10)
        self.assertEqual(MIN_SPEED, self.limiter.get_lower_limit(radius))
        self.limiter.update_lower_limit(radius, 5)
        self.assertEqual(5, self.limiter.get_lower_limit(radius))
        self.assertEqual(10, self.limiter.get_upper_limit(radius))

    def test_updating_upper_limit_with_existing_lower_limit(self):
        radius = 150
        self.limiter.update_lower_limit(radius, 5)
        self.assertEqual(MAX_SPEED, self.limiter.get_upper_limit(radius))
        self.limiter.update_upper_limit(radius, 10)
        self.assertEqual(10, self.limiter.get_upper_limit(radius))
        self.assertEqual(5, self.limiter.get_lower_limit(radius))

    def test_getting_upper_limit_for_radius_smaller_than_inserted(self):
        self.limiter.update_upper_limit(100, 5)
        self.limiter.update_upper_limit(75, 4)
        self.limiter.update_upper_limit(125, 6)
        self.assertEqual(4, self.limiter.get_upper_limit(50))
        self.limiter.update_lower_limit(60, 2)
        self.assertEqual(4, self.limiter.get_upper_limit(50))

    def test_getting_lower_limit_for_radius_bigger_than_inserted(self):
        self.limiter.update_lower_limit(100, 5)
        self.limiter.update_lower_limit(125, 6)
        self.limiter.update_lower_limit(75, 4)
        self.assertEqual(6, self.limiter.get_lower_limit(150))
        self.limiter.update_upper_limit(140, 10)
        self.assertEqual(6, self.limiter.get_lower_limit(150))

    def test_updating_lower_limit_greater_than_upper_limit(self):
        self.limiter.update_upper_limit(100, 10)
        self.limiter.update_lower_limit(100, 5)
        self.assertEqual(5, self.limiter.get_lower_limit(100))
        self.limiter.update_lower_limit(100, 11)
        self.assertEqual(10, self.limiter.get_lower_limit(100))

    def test_updating_upper_limit_lower_than_lower_limit(self):
        self.limiter.update_upper_limit(100, 10)
        self.limiter.update_lower_limit(100, 5)
        self.assertEqual(10, self.limiter.get_upper_limit(100))
        self.limiter.update_upper_limit(100, 4)
        self.assertEqual(4, self.limiter.get_upper_limit(100))
        self.assertEqual(4, self.limiter.get_lower_limit(100))

    def test_updating_upper_limit_should_also_update_upper_limit_for_smaller_radiuses(self):
        self.limiter.update_upper_limit(100, 10)
        self.limiter.update_upper_limit(75, 7)
        self.limiter.update_upper_limit(50, 5)
        self.limiter.update_upper_limit(125, 15)

        self.limiter.update_lower_limit(50, 1)
        self.limiter.update_lower_limit(75, 2)
        self.limiter.update_lower_limit(100, 3)
        self.limiter.update_lower_limit(125, 4)

        self.limiter.update_upper_limit(100, 6)
        self.assertEqual(6, self.limiter.get_upper_limit(100))
        self.assertEqual(6, self.limiter.get_upper_limit(75))
        self.assertEqual(5, self.limiter.get_upper_limit(50))
        self.assertEqual(15, self.limiter.get_upper_limit(125))

        self.assertEqual(5, self.limiter.get_lower_limit(50))
        self.assertEqual(5, self.limiter.get_lower_limit(75))
        self.assertEqual(5, self.limiter.get_lower_limit(100))
        self.assertEqual(5, self.limiter.get_lower_limit(125))

    def test_updating_lower_limit_should_also_update_lower_limit_for_bigger_radiuses(self):
        self.limiter.update_lower_limit(50, 3)
        self.limiter.update_lower_limit(100, 10)
        self.limiter.update_lower_limit(125, 13)
        self.limiter.update_lower_limit(150, 15)
        self.limiter.update_lower_limit(200, 25)

        self.limiter.update_upper_limit(50, 50)
        self.limiter.update_upper_limit(100, 51)
        self.limiter.update_upper_limit(125, 52)
        self.limiter.update_upper_limit(150, 53)
        self.limiter.update_upper_limit(200, 54)

        self.limiter.update_lower_limit(100, 20)
        self.assertEqual(20, self.limiter.get_lower_limit(100))
        self.assertEqual(3, self.limiter.get_lower_limit(50))
        self.assertEqual(20, self.limiter.get_lower_limit(125))
        self.assertEqual(20, self.limiter.get_lower_limit(150))
        self.assertEqual(25, self.limiter.get_lower_limit(200))

        self.assertEqual(50, self.limiter.get_upper_limit(50))
        self.assertEqual(51, self.limiter.get_upper_limit(100))
        self.assertEqual(52, self.limiter.get_upper_limit(125))
        self.assertEqual(53, self.limiter.get_upper_limit(150))
        self.assertEqual(54, self.limiter.get_upper_limit(200))


if __name__ == '__main__':
    unittest.main()
