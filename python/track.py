import math

MAX_POSSIBLE_SPEED = 50
POSITION_TOLERANCE = 10
INPIECEDISTANCE_INTERVAL = 25
MAX_SPEED = 100
MIN_SPEED = 1


class Track(object):
    def __init__(self, track_data):
        self.id = track_data["id"]
        self.name = track_data["name"]
        self.lane_data = track_data["lanes"]
        self.laps = None
        self.current_index = 0
        self.previous_index = 0

        self.pieces = [Piece.create_piece(piece_data, self.lane_data) for piece_data in track_data["pieces"]]

    def average_length(self):
        lane_lengths = [sum([piece.get_length(lane_index) for piece in self.pieces]) for lane_index, lane in enumerate(self.lane_data)]
        return sum(lane_lengths) * 1.0 / len(lane_lengths)

    def lap_length(self, lane):
        return sum([piece.get_length(lane) for piece in self.pieces])

    def lap_distance(self, lane, pieceIndex, inPieceDistance):
        distance = 0
        for index in range(0, pieceIndex):
            distance += self.pieces[index].get_length(lane)
        return distance + inPieceDistance

    def remaining_lap_distance(self, lane, pieceIndex, inPieceDistance):
        distance = 0
        for index in range(pieceIndex + 1, len(self.pieces)):
            distance += self.pieces[index].get_length(lane)
        return distance + self.pieces[pieceIndex].get_remaining_length(lane, inPieceDistance)

    def get_shortest_lane_between_pieces(self, index1, index2):
        lane_indices = range(0, len(self.lane_data))
        distances = [(lane, self.get_lane_distance_between_pieces(lane, index1, index2)) for lane in lane_indices]
        if len(set([d[1] for d in distances])) == 1:
            # All lanes equally long
            return None
        return min(distances, key=lambda x: x[1])[0]

    def get_lane_distance_between_pieces(self, lane, index1, index2):
        if index1 < index2:
            indices_between = range(index1, index2)
        else:
            indices_between = range(index1, len(self.pieces)) + range(0, index2)
        pieces = [piece for index, piece in enumerate(self.pieces) if index in indices_between]
        return sum([piece.get_length(lane) for piece in pieces])

    def get_index_of_next_switch_piece(self, current_index):
        all_indices = range(current_index + 1, len(self.pieces)) + range(0, current_index + 1)
        switch_pieces = [index for index in all_indices if self.pieces[index].is_switch()]
        if switch_pieces != []:
            return switch_pieces[0]
        return None

    def get_straight_piece_indeces(self):
        return [i for i, p in enumerate(self.pieces) if type(p) == Straight]

    def get_continuous_straight_regions(self):
        regions = []
        r = []
        for index in self.get_straight_piece_indeces():
            if r and index != r[-1] + 1:
                regions.append(r)
                r = []
            r.append(index)
        regions.append(r)
        if len(regions) > 1 and regions[0][0] == 0 and regions[-1][-1] == len(self.pieces) - 1:
            regions[0] = regions.pop() + regions[0]
        return regions

    def find_longest_continuous_straight(self):
        longest = (-1, [])
        regions = self.get_continuous_straight_regions()
        for region in regions:
            s = sum(map(lambda i: self.pieces[i].length, region))
            if s > longest[0]:
                longest = (s, region)
        return longest

    def get_nth_piece_after(self, index, n):
        return self.pieces[(index + n) % len(self.pieces)]

    def is_at_beginning_of_longest_straight(self, index, inPieceDistance):
        longest = self.find_longest_continuous_straight()
        if longest is None:
            return False
        length, indices = longest
        if indices == []:
            return False
        return index == indices[0] and inPieceDistance < POSITION_TOLERANCE

    def on_final_straight(self, index, lap):
        if not self.on_final_lap(lap):
            return False
        straights = self.get_continuous_straight_regions()
        last_index = len(self.pieces) - 1
        for s in straights:
            if last_index in s and index in s:
                return True
        return False

    def on_final_lap(self, lap):
        if not self.laps:
            return False
        if lap != self.laps - 1:
            return False
        return True

    def on_beginning_of_final_straight(self, index, lap):
        if not self.on_final_lap(lap):
            return False
        straights = self.get_continuous_straight_regions()
        last_index = len(self.pieces) - 1
        for region in straights:
            if last_index in region and index == region[0]:
                return True
        return False

    def update_position(self, pieceIndex, angle):
        piece = self.pieces[pieceIndex]
        piece.add_slip_angle(angle)
        self.previous_index = self.current_index
        self.current_index = pieceIndex

    def previous_piece(self):
        return self.pieces[self.previous_index]

    def get_possible_lanes_to_switch(self, lane_index):
        lanes = [lane["index"] for lane in self.lane_data]
        possible_lanes = []
        if lane_index - 1 in lanes:
            possible_lanes.append(lane_index - 1)
        if lane_index + 1 in lanes:
            possible_lanes.append(lane_index + 1)
        return possible_lanes

    def get_piece_from_position_with_offset(self, lane, index, in_piece_distance, offset):
        if offset > 0:
            remaining = self.pieces[index].get_remaining_length(0, in_piece_distance)
            if offset < remaining:
                return (index, in_piece_distance + offset)
            else:
                return self.get_piece_from_position_with_offset(lane, (index + 1) % len(self.pieces), 0, offset - remaining)
        elif offset < 0:
            if -offset < in_piece_distance:
                return (index, in_piece_distance + offset)
            else:
                i = (index - 1) % len(self.pieces)
                distance = self.pieces[i].get_length(lane)
                return self.get_piece_from_position_with_offset(lane, i, distance, offset + in_piece_distance)
        else:
            return (index, in_piece_distance)


class Piece(object):
    @staticmethod
    def create_piece(piece_data, lane_data):
        lanes = [lane["distanceFromCenter"] for lane in lane_data]  # TODO, make sure indexing is right
        switch = False
        if "switch" in piece_data and piece_data["switch"] is True:
            switch = True
        if "length" in piece_data:
            return Straight(piece_data["length"], switch)
        elif "radius" in piece_data:
            return Bend(piece_data["radius"], piece_data["angle"], lanes, switch)

    def __init__(self):
        self.slip_angles = []
        speed_limit_points = (range(0, int(self.get_length(0) / INPIECEDISTANCE_INTERVAL)) * INPIECEDISTANCE_INTERVAL) # TODO: lane is not always 0...
        self.speed_limits = {point: (MIN_SPEED, MAX_SPEED) for point in speed_limit_points}

    def get_nearest_speedlimit_point(self, inPieceDistance):
        return int(inPieceDistance / INPIECEDISTANCE_INTERVAL)

    def get_length(self, lane):
        raise NotImplementedError()

    def get_remaining_length(self, lane, in_piece_distance):
        return self.get_length(lane) - in_piece_distance

    def add_slip_angle(self, angle):
        self.slip_angles.append(angle)

    def get_maximum_absolute_slip_angle(self):
        try:
            return max([abs(angle) for angle in self.slip_angles])
        except ValueError:
            return None

    def reset_slip_angle(self):
        self.slip_angles = []

    def update_lower_speed_limit(self, inPieceDistance, speed, laneIndex):
        nearest_point = self.get_nearest_speedlimit_point(inPieceDistance)
        old_lower_limit = self.speed_limits[nearest_point][0]
        old_upper_limit = self.speed_limits[nearest_point][1]
        new_lower_limit = min(max(old_lower_limit, speed), old_upper_limit)
        self.speed_limits[nearest_point] = (new_lower_limit, old_upper_limit)

    def update_upper_speed_limit(self, inPieceDistance, speed, laneIndex):
        nearest_point = self.get_nearest_speedlimit_point(inPieceDistance)
        old_lower_limit = self.speed_limits(nearest_point)[0]
        old_upper_limit = self.speed_limits[nearest_point][1]
        new_upper_limit = min(old_upper_limit, speed)  # minimum, because the upper limit is updated only when a crash happens
        new_lower_limit = min(old_lower_limit, new_upper_limit)
        self.speed_limits[nearest_point] = (new_lower_limit, new_upper_limit)

    def __str__(self):
        raise NotImplementedError()


class Straight(Piece):
    def __init__(self, length, switch=False):
        self.length = length
        self.switch = switch
        super(Straight, self).__init__()

    def get_length(self, lane):
        return self.length

    def is_switch(self):
        return self.switch

    def __str__(self):
        return "S: %d" % self.length


class Bend(Piece):
    def __init__(self, radius, angle, lanes, switch=False):
        self.radius = radius
        self.angle = angle
        self.lanes = lanes
        self.switch = switch
        self.length = {index: self.__calculate_length(index) for index, lane in enumerate(lanes)}
        super(Bend, self).__init__()

    def get_length(self, lane):
        return self.length[lane]

    def is_switch(self):
        return self.switch

    def get_radius(self, lane):
        radius_offset = self.lanes[lane]
        return self.radius + radius_offset

    def __str__(self):
        return "B: r: %d, a: %d" % (self.radius, self.angle)

    def __calculate_length(self, lane):
        if self.angle < 0:
            # Left turn
            radius_offset = self.lanes[lane]
            angle = -self.angle
        else:
            # Right turn
            radius_offset = -self.lanes[lane]
            angle = self.angle

        return 2 * math.pi * (self.radius + radius_offset) * angle / 360.0
