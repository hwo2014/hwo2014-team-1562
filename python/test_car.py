import unittest
from car import Car, CarPosition, DEFAULT_CRASH_ANGLE
from track import Track


class TestCar(unittest.TestCase):
    def setUp(self):
        self.car_data = {
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "dimensions": {
                "length": 40.0,
                "width": 20.0,
                "guideFlagPosition": 10.0
            }
        }

        self.position_data = [{
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 0.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }, {
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "angle": 10.0,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 10.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }, {
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 2,
                "inPieceDistance": 10.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }, {
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 25.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 1
            }
        }]

        self.track = Track({
            "id": "indianapolis",
            "name": "Indianapolis",
            "pieces": [
                {
                    "length": 100.0
                },
                {
                    "length": 100.0,
                },
                {
                    "length": 50.0,
                }
            ],
            "lanes": [
                {
                    "distanceFromCenter": -20,
                    "index": 0
                },
                {
                    "distanceFromCenter": 0,
                    "index": 1
                },
                {
                    "distanceFromCenter": 20,
                    "index": 2
                }
            ],
            "startingPoint": {
                "position": {
                    "x": -340.0,
                    "y": -96.0
                },
                "angle": 90.0
            }
        })

    def test_update_position(self):
        pos1 = self.position_data[0]
        car = Car(self.car_data, self.track)
        # initial position
        car.update_position(pos1, 1)
        self.assertIsNotNone(car.position)
        self.assertEqual(car.position.tick, 1)
        self.assertEqual(0, car.position.speed)
        self.assertEqual(0, car.position.acceleration)

        # update within same track piece
        pos2 = pos1.copy()
        pos2["piecePosition"]["inPieceDistance"] += 50
        car.update_position(pos2, 2)
        self.assertAlmostEqual(car.position.speed, 50)
        self.assertEqual(0, car.position.acceleration)

        # moving to next track piece
        pos3 = pos2.copy()
        pos3["piecePosition"]["pieceIndex"] += 1
        car.update_position(pos3, 3)
        self.assertAlmostEqual(car.position.speed, 100)
        self.assertAlmostEqual(car.position.acceleration, 50)
        # TODO: moving to next lap

    def test_distance_to_in_same_piece(self):
        position_1 = CarPosition(self.track)
        position_1.update(self.position_data[0], 0)
        position_2 = CarPosition(self.track)
        position_2.update(self.position_data[1], 0)
        distance = position_1.distance_to(position_2)
        self.assertEqual(distance, 10)

    def test_distance_to_different_pieces(self):
        position_1 = CarPosition(self.track)
        position_1.update(self.position_data[1], 0)
        position_2 = CarPosition(self.track)
        position_2.update(self.position_data[2], 0)
        distance = position_1.distance_to(position_2)
        self.assertEqual(distance, 200)

    def test_distance_to_different_laps(self):
        position_1 = CarPosition(self.track)
        position_1.update(self.position_data[2], 0)
        position_2 = CarPosition(self.track)
        position_2.update(self.position_data[3], 0)
        distance = position_1.distance_to(position_2)
        self.assertEqual(distance, 65)

    def test_learning_crash_angle(self):
        car = Car(self.car_data, self.track)
        self.assertEqual(DEFAULT_CRASH_ANGLE, car.get_crash_angle())
        angle = 56.7
        crash_position_data = {
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "angle": angle,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 0.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }
        car.update_position(crash_position_data, 1)
        car.crash()
        self.assertEqual(angle, car.get_crash_angle())

        crash_position_data["angle"] = 61
        car.update_position(crash_position_data, 1)
        car.crash()
        self.assertEqual(angle, car.get_crash_angle())

        crash_position_data["angle"] = -45.5
        car.update_position(crash_position_data, 1)
        car.crash()
        self.assertEqual(45.5, car.get_crash_angle())

    def test_update_position_with_invalid_data(self):
        car = Car(self.car_data, self.track)
        invalid_data = {
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 0.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }
        car.update_position(invalid_data, 1)


if __name__ == '__main__':
    unittest.main()
