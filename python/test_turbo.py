import unittest
from turbo import Turbo


class TestTurbo(unittest.TestCase):
    def setUp(self):
        self.turbo_data = {
            "turboDurationMilliseconds": 500.0,
            "turboDurationTicks": 30,
            "turboFactor": 3.0
        }
        self.turbo = Turbo()

    def test_initial_unavailability(self):
        self.assertFalse(self.turbo.is_available())

    def test_setting_and_getting_availability(self):
        self.turbo.set_turbo_available(self.turbo_data)
        self.assertTrue(self.turbo.is_available())

    def test_unavailability_after_use(self):
        self.turbo.set_turbo_available(self.turbo_data)
        self.assertTrue(self.turbo.is_available())
        self.turbo.on_start()
        self.assertFalse(self.turbo.is_available())

    def test_turbo_availibility_when_crashed(self):
        self.assertFalse(self.turbo.is_available())

        self.turbo.on_crash()
        self.turbo.set_turbo_available(self.turbo_data)
        self.assertFalse(self.turbo.is_available())

        self.turbo.on_spawn()
        self.assertFalse(self.turbo.is_available())

        self.turbo.set_turbo_available(self.turbo_data)
        self.assertTrue(self.turbo.is_available())

    def test_invalidate_turbo_with_crash(self):
        self.turbo.set_turbo_available(self.turbo_data)
        self.assertTrue(self.turbo.is_available())
        self.turbo.on_crash()
        self.assertFalse(self.turbo.is_available())
        self.turbo.on_spawn()
        self.assertFalse(self.turbo.is_available())

    def test_enabled_state(self):
        self.assertFalse(self.turbo.is_enabled())
        self.turbo.set_turbo_available(self.turbo_data)
        self.assertFalse(self.turbo.is_enabled())
        self.turbo.on_start()
        self.assertTrue(self.turbo.is_enabled())
        self.turbo.on_end()
        self.assertFalse(self.turbo.is_enabled())

        self.turbo.set_turbo_available(self.turbo_data)
        self.assertFalse(self.turbo.is_enabled())
        self.turbo.on_start()
        self.assertTrue(self.turbo.is_enabled())
        self.turbo.on_crash()
        self.assertFalse(self.turbo.is_enabled())
        self.turbo.on_end()
        self.assertFalse(self.turbo.is_enabled())


if __name__ == '__main__':
    unittest.main()
