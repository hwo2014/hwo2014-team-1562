import unittest
from pid import PID


class TestPID(unittest.TestCase):
    def setUp(self):
        self.pid = PID(1, 0, 0, 2, 0, 1)

    def test_pid_output_value_for_p_controller(self):
        control = self.pid.run(1.9, 1)
        self.assertAlmostEqual(0.1, control)

    def test_pid_output_value_for_pd_controller(self):
        self.pid.set_kd(2)
        control = self.pid.run(1.9, 1)
        self.assertAlmostEqual(0.3, control)

    def test_pid_output_value_for_pi_controller(self):
        self.pid.set_ki(2)
        control = self.pid.run(1.9, 1)
        control = self.pid.run(1.9, 1)
        control = self.pid.run(1.9, 1)
        self.assertAlmostEqual(0.7, control)

    def test_pid_max_output(self):
        control = self.pid.run(0, 1)
        self.assertAlmostEqual(1, control)

    def test_pid_min_output(self):
        control = self.pid.run(2.1, 1)
        self.assertAlmostEqual(0, control)

    def test_pid_integrator_antiwindup(self):
        self.pid.set_ki(5)
        for i in range(0, 10):
            control = self.pid.run(1.9, 1)
        self.assertAlmostEqual(1, control)

    def test_reseting_integrator_when_setpoint_changes(self):
        self.pid.integral = 1
        self.assertEqual(1, self.pid.integral)
        self.pid.set_setpoint(1.4)
        self.assertEqual(0, self.pid.integral)
        self.pid.integral = 1
        self.assertEqual(1, self.pid.integral)
        self.pid.set_setpoint(1.4)
        self.assertEqual(1, self.pid.integral)


if __name__ == '__main__':
    unittest.main()
