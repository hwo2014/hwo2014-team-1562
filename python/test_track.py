import unittest
from track import Straight, Bend, Track, Piece, POSITION_TOLERANCE
import math


class TestPieces(unittest.TestCase):
    def setUp(self):
        self.lane_data = [{
            "distanceFromCenter": -20,
            "index": 0
        }, {
            "distanceFromCenter": 0,
            "index": 1
        }, {
            "distanceFromCenter": 20,
            "index": 2
        }]

        self.track_data = {
            "id": "indianapolis",
            "name": "Indianapolis",
            "pieces": [
                {
                    "length": 100.0
                },
                {
                    "length": 25.0,
                },
                {
                    "length": 50.0,
                },
            ],
            "lanes": [
                {
                    "distanceFromCenter": -20,
                    "index": 0
                },
                {
                    "distanceFromCenter": 0,
                    "index": 1
                },
                {
                    "distanceFromCenter": 20,
                    "index": 2
                }
            ],
            "startingPoint": {
                "position": {
                    "x": -340.0,
                    "y": -96.0
                },
                "angle": 90.0
            }
        }

    def test_straight_length(self):
        length = 123
        piece = Straight(length)
        self.assertEqual(length, piece.get_length(1))

    def test_creating_straight_piece(self):
        length = 42
        piece = Piece.create_piece({"length": length}, self.lane_data)
        self.assertEqual(length, piece.get_length(1))

    def test_creating_bend_piece(self):
        radius = 100
        angle = 360
        piece = Piece.create_piece({"radius": radius, "angle": angle}, self.lane_data)
        self.assertAlmostEqual(2 * math.pi * (radius + 20), piece.get_length(0))
        self.assertAlmostEqual(2 * math.pi * radius, piece.get_length(1))
        self.assertAlmostEqual(2 * math.pi * (radius - 20), piece.get_length(2))

    def test_bend_length(self):
        radius = 50
        angle = -30
        piece = Bend(radius, angle, [-10, 0, 10])
        self.assertAlmostEqual(20.943951023932, piece.get_length(0))
        self.assertAlmostEqual(26.179938779915, piece.get_length(1))
        self.assertAlmostEqual(31.415926535898, piece.get_length(2))

    def test_straight_remaining_length(self):
        piece = Straight(120)
        self.assertEqual(50, piece.get_remaining_length(1, 70))

    def test_bend_remaining_length(self):
        radius = 50
        angle = -30
        piece = Bend(radius, angle, [-10, 0, 10])
        self.assertAlmostEqual(10.943951023932, piece.get_remaining_length(0, 10))

    def test_calculating_lap_distance(self):
        t = Track(self.track_data)
        self.assertEqual(100 + 25 + 10, t.lap_distance(0, 2, 10))

    def test_calculating_remaining_lap_distance(self):
        t = Track(self.track_data)
        self.assertEqual(50 + 15, t.remaining_lap_distance(0, 1, 10))

    def test_calculating_lap_length(self):
        t = Track(self.track_data)
        self.assertEqual(100 + 25 + 50, t.lap_length(0))

    def test_calculating_lane_distance_between_pieces(self):
        t = Track(self.track_data)
        t.pieces = [Straight(100), Straight(50), Straight(25), Straight(10)]
        self.assertEqual(75, t.get_lane_distance_between_pieces(0, 1, 3))
        self.assertEqual(110, t.get_lane_distance_between_pieces(0, 3, 1))
        self.assertEqual(185, t.get_lane_distance_between_pieces(0, 0, 0))
        self.assertEqual(185, t.get_lane_distance_between_pieces(0, 1, 1))

    def test_calculating_shortest_lane_between_pieces(self):
        t = Track(self.track_data)
        lanes = [-10, 0, 10]
        t.pieces = [Bend(100, 90, lanes), Bend(100, 90, lanes), Bend(50, -10, lanes),
                    Bend(50, -10, lanes), Bend(50, -10, lanes), Bend(100, -90, lanes),
                    Bend(100, -80, lanes), Bend(100, -90, lanes)]
        self.assertEqual(2, t.get_shortest_lane_between_pieces(0, 2))
        self.assertEqual(0, t.get_shortest_lane_between_pieces(2, 0))
        self.assertEqual(0, t.get_shortest_lane_between_pieces(0, 0))

    def test_getting_next_switch_piece_index(self):
        t = Track(self.track_data)
        t.pieces = [Straight(100, True), Straight(50), Straight(25), Straight(10, True), Straight(50), Straight(10)]
        self.assertEqual(3, t.get_index_of_next_switch_piece(1))
        self.assertEqual(3, t.get_index_of_next_switch_piece(0))
        self.assertEqual(0, t.get_index_of_next_switch_piece(3))
        self.assertEqual(3, t.get_index_of_next_switch_piece(0))
        t.pieces = [Straight(100), Straight(50), Straight(25), Straight(10, True), Straight(50), Straight(10)]
        self.assertEqual(3, t.get_index_of_next_switch_piece(1))
        self.assertEqual(3, t.get_index_of_next_switch_piece(4))
        self.assertEqual(3, t.get_index_of_next_switch_piece(3))
        t.pieces = [Straight(100, True), Straight(50), Straight(25), Straight(10), Straight(50), Straight(10)]
        self.assertEqual(0, t.get_index_of_next_switch_piece(0))
        t.pieces = [Straight(100), Straight(50), Straight(25), Straight(10), Straight(50), Straight(10, True)]
        self.assertEqual(5, t.get_index_of_next_switch_piece(5))
        t.pieces = [Straight(100), Straight(50), Straight(25), Straight(10), Straight(50), Straight(10)]
        self.assertEqual(None, t.get_index_of_next_switch_piece(5))

    def test_find_longest_continuous_straight(self):
        data = self.track_data.copy()
        t = Track(data)
        self.assertEqual((175, [0, 1, 2]), t.find_longest_continuous_straight())
        data["pieces"].append({"radius": 100, "angle": 90})

        t = Track(data)
        self.assertEqual((175, [0, 1, 2]), t.find_longest_continuous_straight())
        data["pieces"].append({"length": 100})
        data["pieces"].append({"length": 100})
        t = Track(data)
        self.assertEqual((375, [4, 5, 0, 1, 2]), t.find_longest_continuous_straight())

        data["pieces"].append({"radius": 100, "angle": 45})
        t = Track(data)
        self.assertEqual((200, [4, 5]), t.find_longest_continuous_straight())

    def test_getting_bend_radius(self):
        piece = Bend(50, 90, [-10, 0, 10])
        self.assertEqual(40, piece.get_radius(0))
        self.assertEqual(40, piece.get_radius(0))
        self.assertEqual(60, piece.get_radius(2))

    def test_getting_nth_piece_after(self):
        t = Track(self.track_data)
        p1 = Straight(100)
        p2 = Straight(50)
        p3 = Straight(25)
        p4 = Straight(12.5)
        t.pieces = [p1, p2, p3, p4]
        self.assertEqual(p2, t.get_nth_piece_after(0, 1))
        self.assertEqual(p1, t.get_nth_piece_after(2, 2))
        self.assertEqual(p4, t.get_nth_piece_after(3, 0))

    def test_is_at_beginning_of_longest_straight(self):
        t = Track(self.track_data)
        self.assertEqual((175, [0, 1, 2]), t.find_longest_continuous_straight())
        self.assertTrue(t.is_at_beginning_of_longest_straight(0, 0))
        self.assertTrue(t.is_at_beginning_of_longest_straight(0, 7))
        self.assertFalse(t.is_at_beginning_of_longest_straight(0, POSITION_TOLERANCE + 1))
        self.assertFalse(t.is_at_beginning_of_longest_straight(1, 0))

    def test_on_final_straight(self):
        data = self.track_data.copy()
        data["pieces"] = [{"radius": 100, "angle": 45}] + data["pieces"]
        t = Track(data)
        t.laps = 3
        self.assertTrue(t.on_final_straight(1, 2))
        self.assertTrue(t.on_final_straight(2, 2))
        self.assertFalse(t.on_final_straight(1, 1))
        self.assertFalse(t.on_final_straight(0, 2))

    def test_on_final_lap(self):
        t = Track(self.track_data)
        t.laps = 3
        self.assertFalse(t.on_final_lap(0))
        self.assertFalse(t.on_final_lap(1))
        self.assertTrue(t.on_final_lap(2))

    def test_updating_measured_bend_slip_angle(self):
        bend = Bend(100, 90, [-10, 0, 10])
        self.assertIsNone(bend.get_maximum_absolute_slip_angle())
        bend.add_slip_angle(10)
        bend.add_slip_angle(5)
        bend.add_slip_angle(-5)
        self.assertEqual(10, bend.get_maximum_absolute_slip_angle())
        bend.add_slip_angle(-15)
        self.assertEqual(15, bend.get_maximum_absolute_slip_angle())
        bend.reset_slip_angle()
        self.assertIsNone(bend.get_maximum_absolute_slip_angle())

    def test_getting_possible_lanes_to_switch_to(self):
        t = Track(self.track_data)
        self.assertEqual([1], t.get_possible_lanes_to_switch(0))
        self.assertEqual([0, 2], t.get_possible_lanes_to_switch(1))
        self.assertEqual([1], t.get_possible_lanes_to_switch(2))
        t.lane_data = [
            {
                "distanceFromCenter": -20,
                "index": 0
            },
            {
                "distanceFromCenter": 0,
                "index": 1
            },
            {
                "distanceFromCenter": 20,
                "index": 2
            },
            {
                "distanceFromCenter": 30,
                "index": 3
            }
        ]
        self.assertEqual([1, 3], t.get_possible_lanes_to_switch(2))
        self.assertEqual([2], t.get_possible_lanes_to_switch(3))
        t.lane_data = [
            {
                "distanceFromCenter": 0,
                "index": 0
            }
        ]
        self.assertEqual([], t.get_possible_lanes_to_switch(0))

    def test_getting_piece_from_position_with_offset(self):
        pieces = [
                {
                    "length": 100.0
                },
                {
                    "length": 25.0,
                },
                {
                    "length": 50.0,
                },
                {
                    "length": 75.0,
                },
        ]
        data = self.track_data.copy()
        data["pieces"] = pieces
        t = Track(data)
        self.assertEqual((0, 0), t.get_piece_from_position_with_offset(0, 0, 0, 0))
        self.assertEqual((3, 10), t.get_piece_from_position_with_offset(0, 3, 10, 0))
        self.assertEqual((2, 40), t.get_piece_from_position_with_offset(0, 2, 10, 30))
        self.assertEqual((3, 0), t.get_piece_from_position_with_offset(0, 2, 10, 40))
        self.assertEqual((3, 60), t.get_piece_from_position_with_offset(0, 2, 10, 100))
        self.assertEqual((0, 35), t.get_piece_from_position_with_offset(0, 2, 10, 150))
        self.assertEqual((3, 74), t.get_piece_from_position_with_offset(0, 0, 0, -1))
        self.assertEqual((0, 50), t.get_piece_from_position_with_offset(0, 0, 0, -200))


if __name__ == '__main__':
    unittest.main()
