import logging
import track
from speedcontroller import SpeedController
from turbo import Turbo

DEBUG_TICKS = 5
DEFAULT_CRASH_ANGLE = 90


class Car(object):
    def __init__(self, car_data, track, speed_controller=None):
        self.name = car_data["id"]["name"]
        self.color = car_data["id"]["color"]
        self.length = car_data["dimensions"]["length"]
        self.width = car_data["dimensions"]["width"]
        self.guideFlagPosition = car_data["dimensions"]["guideFlagPosition"]
        self.track = track
        self.position = CarPosition(self.track)
        if speed_controller:
            self.speed_controller = speed_controller
        else:
            self.speed_controller = SpeedController(self.track)
        self.turbo = Turbo()
        self.reset()
        self.crash_angle = DEFAULT_CRASH_ANGLE

    def reset(self):
        self.throttle = 0.5
        self.training_crashes = 5
        self.position.reset()

    def update_position(self, position_data, tick):
        last_piece_index = self.position.pieceIndex
        try:
            self.position.update(position_data, tick)
        except:
            logging.debug("Invalid position data:" + str(position_data))

        if last_piece_index != self.position.pieceIndex:
            logging.debug("Piece changed, updating speed limit with car " + self.color)
            self.speed_controller.on_track_piece_change(self.track.pieces[last_piece_index], last_piece_index, self.position, self.crash_angle)

        if tick % DEBUG_TICKS == 0 and self.position.speed > 0.1:
            target_speed = self.speed_controller.get_target_speed(self.position)
            piece = self.track.pieces[self.position.pieceIndex]
            logging.debug("tick: %6d, color: %s, pos: %3d - %3d (%s), lane: %d, vel: %.3f (target: %.3f), acc: %.3f throttle: %.3f, slip_angle: %2.3f"
                          % (self.position.tick, self.color, self.position.pieceIndex, self.position.inPieceDistance, piece, self.position.lane, self.position.speed, target_speed, self.position.acceleration, self.throttle, self.position.angle))

    def get_throttle(self):
        self.throttle = self.speed_controller.update_throttle(self.position)
        return self.throttle

    def crash(self):
        piece = self.position.get_current_trackpiece()
        self.speed_controller.on_crash(self.position.speed, self.position)
        self.crash_angle = min(self.crash_angle, abs(self.position.angle))
        if isinstance(piece, track.Bend):
            radius = piece.radius
            logging.debug("This car crashed, speed=%f, prev_speed=%f, slip angle=%f, bend radius=%f, throttle=%f"
                          % (self.position.speed, self.position.prev_speed, self.position.angle, radius, self.throttle))
        logging.debug("New learned maximum crash angle: %.3f" % self.crash_angle)
        self.turbo.on_crash()

    def get_crash_angle(self):
        return self.crash_angle

    def update_turbo(self, turbo_data):
        self.turbo.set_turbo_available(turbo_data)

    def use_turbo(self, msg_function):
        return self.turbo.is_available() and self.turbo.should_use_turbo_here(self.track, self.position)


class CarPosition(object):
    def __init__(self, track):
        self.prev_position = None
        self.angle = 0
        self.pieceIndex = 0
        self.inPieceDistance = 0
        self.lap = 0
        self.lane = 0
        self.prev_lane = 0
        self.track = track
        self.reset()

    def reset(self):
        self.speed = 0
        self.prev_speed = 0
        self.acceleration = 0
        self.tick = 0
        self.prev_tick = 0

    def update(self, position_data, tick):
        if self.prev_position is None:
            self.prev_position = CarPosition(self.track)
        self.prev_position.angle = self.angle
        self.prev_position.pieceIndex = self.pieceIndex
        self.prev_position.inPieceDistance = self.inPieceDistance
        self.prev_position.lap = self.lap
        self.prev_position.lane = self.lane
        self.prev_position.prev_lane = self.prev_lane
        self.prev_position.speed = self.speed
        self.prev_position.prev_speed = self.prev_speed
        self.prev_position.acceleration = self.acceleration
        self.prev_position.tick = self.tick
        self.prev_position.prev_tick = self.prev_tick

        self.angle = position_data["angle"]
        piece_position = position_data["piecePosition"]
        self.pieceIndex = piece_position["pieceIndex"]
        self.inPieceDistance = piece_position["inPieceDistance"]
        self.lap = piece_position["lap"]
        self.lane = piece_position["lane"]["endLaneIndex"]
        self.prev_lane = piece_position["lane"]["startLaneIndex"]

        self.prev_tick = self.tick
        self.tick = tick
        self.update_speed()
        self.update_acceleration()
        self.track.update_position(self.pieceIndex, self.angle)

    def delta_tick(self):
        return self.tick - self.prev_tick

    def update_speed(self):
        self.prev_speed = self.speed
        distance = self.prev_position.distance_to(self)
        if self.delta_tick():
            self.speed = 1.0 * distance / self.delta_tick()

    def update_acceleration(self):
        if self.prev_speed and self.delta_tick():
            self.acceleration = (self.speed - self.prev_speed) / self.delta_tick()

    def get_current_trackpiece(self):
        return self.track.pieces[self.pieceIndex]

    def is_on_same_piece(self, other):
        return self.pieceIndex == other.pieceIndex

    def real_distance_to(self, position_2):
        if self.pieceIndex <= position_2.pieceIndex:
            return self.same_lap_distance_to(position_2)
        else:
            return self.track.lap_length(position_2.lane) - abs(position_2.same_lap_distance_to(self))

    def same_lap_distance_to(self, position_2):
        if self.pieceIndex == position_2.pieceIndex:
            return position_2.inPieceDistance - self.inPieceDistance
        else:
            remaining_in_self = self.track.pieces[self.pieceIndex].get_remaining_length(self.lane, self.inPieceDistance)
            full_piece_lengths = 0
            for i in range(self.pieceIndex + 1, position_2.pieceIndex):
                full_piece_lengths += self.track.pieces[i].get_length(position_2.lane)  # TODO what lane to use here?
            return remaining_in_self + full_piece_lengths + position_2.inPieceDistance

    def distance_to(self, position_2):
        if self.lap == position_2.lap:
            return self.same_lap_distance_to(position_2)
        else:
            full_laps = position_2.lap - self.lap - 1
            full_lap_lengths = full_laps * self.track.lap_length(position_2.lane)
            return (full_lap_lengths
                    + self.track.lap_distance(position_2.lane, position_2.pieceIndex, position_2.inPieceDistance)
                    + self.track.remaining_lap_distance(self.lane, self.pieceIndex, self.inPieceDistance))
