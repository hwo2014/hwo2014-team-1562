from track import Track
from laneswitcher import LaneSwitcher, LEFT, RIGHT
from car import CarPosition
import unittest


class TestLaneSwitcher(unittest.TestCase):
    def setUp(self):
        self.track_data = \
            {u'pieces':
                [{u'length': 100.0},
                 {u'length': 100.0},
                 {u'length': 100.0},
                 {u'length': 100.0, u'switch': True},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 22.5, u'switch': True, u'radius': 200},
                 {u'length': 100.0},
                 {u'length': 100.0},
                 {u'angle': -22.5, u'radius': 200},
                 {u'length': 100.0},
                 {u'length': 100.0, u'switch': True},
                 {u'angle': -45.0, u'radius': 100},
                 {u'angle': -45.0, u'radius': 100},
                 {u'angle': -45.0, u'radius': 100},
                 {u'angle': -45.0, u'radius': 100},
                 {u'length': 100.0, u'switch': True},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 22.5, u'radius': 200},
                 {u'angle': -22.5, u'radius': 200},
                 {u'length': 100.0, u'switch': True},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'length': 62.0},
                 {u'angle': -45.0, u'switch': True, u'radius': 100},
                 {u'angle': -45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'angle': 45.0, u'radius': 100},
                 {u'length': 100.0, u'switch': True},
                 {u'length': 100.0}, {u'length': 100.0},
                 {u'length': 100.0}, {u'length': 90.0}],
             u'lanes':
                [{u'index': 0, u'distanceFromCenter': -10},
                 {u'index': 1, u'distanceFromCenter': 10}],
             u'id': u'keimola',
             u'startingPoint': {u'position': {u'y': -44.0, u'x': -300.0}, u'angle': 90.0}, u'name': u'Keimola'}

        self.track = Track(self.track_data)
        self.switcher = LaneSwitcher(self.track)
        self.current_position = {
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 1,
                "inPieceDistance": 0.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }

    def test_that_we_dont_switch_lanes_when_we_are_on_switch_piece(self):
        self.current_position["piecePosition"]["lane"]["startLaneIndex"] = 0
        self.current_position["piecePosition"]["lane"]["endLaneIndex"] = 0
        self.current_position["piecePosition"]["pieceIndex"] = 3
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertIsNone(self.switcher.should_switch_lane_to(carpos, self.track), [])

    def test_that_we_dont_switch_lanes_when_we_are_already_on_the_shortest_lane(self):
        self.current_position["piecePosition"]["lane"]["startLaneIndex"] = 0
        self.current_position["piecePosition"]["lane"]["endLaneIndex"] = 0
        self.current_position["piecePosition"]["pieceIndex"] = 11
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertIsNone(self.switcher.should_switch_lane_to(carpos, []))
        self.current_position["piecePosition"]["lane"]["startLaneIndex"] = 0
        self.current_position["piecePosition"]["lane"]["endLaneIndex"] = 0
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertIsNone(self.switcher.should_switch_lane_to(carpos, []))

    def test_switching_lane_to_shorter_one(self):
        self.current_position["piecePosition"]["lane"]["startLaneIndex"] = 0
        self.current_position["piecePosition"]["lane"]["endLaneIndex"] = 0
        self.current_position["piecePosition"]["pieceIndex"] = 1
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertEqual(RIGHT, self.switcher.should_switch_lane_to(carpos, []))
        self.current_position["piecePosition"]["lane"]["startLaneIndex"] = 1
        self.current_position["piecePosition"]["lane"]["endLaneIndex"] = 1
        self.current_position["piecePosition"]["pieceIndex"] = 11
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertEqual(LEFT, self.switcher.should_switch_lane_to(carpos, []))
        self.current_position["piecePosition"]["lane"]["startLaneIndex"] = 0
        self.current_position["piecePosition"]["lane"]["endLaneIndex"] = 0
        self.current_position["piecePosition"]["pieceIndex"] = 24
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertEqual(RIGHT, self.switcher.should_switch_lane_to(carpos, []))
        self.current_position["piecePosition"]["pieceIndex"] = 27
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertEqual(RIGHT, self.switcher.should_switch_lane_to(carpos, []))
        self.current_position["piecePosition"]["lane"]["startLaneIndex"] = 0
        self.current_position["piecePosition"]["lane"]["endLaneIndex"] = 0
        self.current_position["piecePosition"]["pieceIndex"] = 36
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertEqual(RIGHT, self.switcher.should_switch_lane_to(carpos, []))

    def test_do_not_switch_if_equal_length(self):
        self.current_position["piecePosition"]["lane"]["startLaneIndex"] = 1
        self.current_position["piecePosition"]["lane"]["endLaneIndex"] = 1
        self.current_position["piecePosition"]["pieceIndex"] = 34
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertEqual(None, self.switcher.should_switch_lane_to(carpos, []))
        self.current_position["piecePosition"]["lane"]["startLaneIndex"] = 0
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.assertEqual(None, self.switcher.should_switch_lane_to(carpos, []))

    def test_that_piece_before_switch_piece_is_correct_piece_to_send_switch_command(self):
        self.assertTrue(self.switcher.is_correct_piece_to_send_switch_command(2))
        self.assertTrue(self.switcher.is_correct_piece_to_send_switch_command(7))

    def test_that_switch_piece_is_not_correct_piece_to_send_switch_command(self):
        self.assertFalse(self.switcher.is_correct_piece_to_send_switch_command(3))
        self.assertFalse(self.switcher.is_correct_piece_to_send_switch_command(8))

    def test_not_correct_pieces_before_switch(self):
        self.assertFalse(self.switcher.is_correct_piece_to_send_switch_command(5))
        self.assertFalse(self.switcher.is_correct_piece_to_send_switch_command(6))
        self.assertFalse(self.switcher.is_correct_piece_to_send_switch_command(9))

    def test_choosing_lane_with_car_in_front_on_the_same_lane(self):
        other_car_positions_data = [{
            "id": {
                "name": "Foo",
                "color": "blue"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 50.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }, {
            "id": {
                "name": "Bar",
                "color": "green"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 1,
                "inPieceDistance": 150.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }]
        carpos = CarPosition(self.track)
        carpos.update(self.current_position, 1)
        self.current_position = carpos

        other_car_positions = []
        for pos_data in other_car_positions_data:
            carpos = CarPosition(self.track)
            carpos.update(pos_data, 1)
            other_car_positions.append(carpos)

        lane = self.switcher.choose_lane(self.current_position, other_car_positions)
        self.assertEqual(1, lane)

    def test_choosing_lane_with_car_in_front_on_different_lane(self):
        current_position_data = {
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 0.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }
        other_car_positions_data = [{
            "id": {
                "name": "Foo",
                "color": "blue"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 50.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 1
                },
                "lap": 0
            }
        }, {
            "id": {
                "name": "Bar",
                "color": "green"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 1,
                "inPieceDistance": 150.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }]
        current_position = CarPosition(self.track)
        current_position.update(current_position_data, 1)
        other_car_positions = []
        for pos_data in other_car_positions_data:
            carpos = CarPosition(self.track)
            carpos.update(pos_data, 1)
            other_car_positions.append(carpos)

        lane = self.switcher.choose_lane(current_position, other_car_positions)
        self.assertEqual(0, lane)

    def test_choosing_lane_with_when_no_cars_in_front(self):
        current_position_data = {
            "id": {
                "name": "Schumacher",
                "color": "red"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 0.0,
                "lane": {
                    "startLaneIndex": 1,
                    "endLaneIndex": 1
                },
                "lap": 0
            }
        }
        other_car_positions_data = [{
            "id": {
                "name": "Foo",
                "color": "blue"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 20.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 1
                },
                "lap": 0
            }
        }, {
            "id": {
                "name": "Bar",
                "color": "green"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 1,
                "inPieceDistance": 150.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }]
        current_position = CarPosition(self.track)
        current_position.update(current_position_data, 1)
        other_car_positions = []
        for pos_data in other_car_positions_data:
            carpos = CarPosition(self.track)
            carpos.update(pos_data, 1)
            other_car_positions.append(carpos)

        lane = self.switcher.choose_lane(current_position, other_car_positions)
        self.assertEqual(0, lane)

if __name__ == '__main__':
    unittest.main()
