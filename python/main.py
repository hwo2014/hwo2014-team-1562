import logging
import json
import socket
import sys
import os
from track import Track
from car import Car
from laneswitcher import LaneSwitcher, LEFT


def config_logging():
    level_num = logging.INFO
    loglevel = os.environ.get("LOGLEVEL")
    if loglevel:
        level_num = getattr(logging, loglevel.upper(), None)

    logging.basicConfig(format="%(levelname)s: %(message)s", level=level_num)


class SirvikoppiBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.color = None
        self.track = None
        self.own_car = None
        self.game_tick = 0
        self.opponent_cars = []
        self.switch_command_sent = False
        self.lane_switcher = None
        self.control_strategy = "PID"

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self, name, track_name=None, car_count=1, password=None):
        data = {"botId": {"name": name, "key": self.key}, "carCount": car_count}
        if track_name is not None:
            data["trackName"] = track_name
        if password is not None:
            data["password"] = password
        logging.debug("Seding joinRace msg: " + str(data))
        return self.msg("joinRace", data)

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def turbo(self):
        logging.debug("Using turbo!")
        self.msg("turbo", "Akuten")

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def run_custom_race(self, track_name, car_count, password):
        self.join_race(self.name, track_name, car_count, password)
        self.msg_loop()

    def on_join(self, data):
        logging.debug("Joined")
        self.ping()

    def on_game_start(self, data):
        logging.debug("Race started")
        self.ping()

    def on_car_positions(self, data):
        own_car_data = self.get_own_car_data(data)
        self.own_car.update_position(own_car_data, self.game_tick)

        # Only one of the commands (throttle, turbo, switchLane) can be sent per tick
        if not self.handle_lane_switching():
            if self.own_car.use_turbo(self.msg):
                self.turbo()
            else:
                self.throttle(self.own_car.get_throttle())

        for opponent_car in self.opponent_cars:
            car_data = self.get_car_data_with_color(data, opponent_car.color)
            opponent_car.update_position(car_data, self.game_tick)

    def handle_lane_switching(self):
        piece = self.own_car.position.pieceIndex
        switch_direction = self.lane_switcher.should_switch_lane_to(self.own_car.position, [x.position for x in self.opponent_cars])
        if not self.switch_command_sent and self.lane_switcher.is_correct_piece_to_send_switch_command(piece) \
                and switch_direction is not None:
            if switch_direction == LEFT:
                logging.debug("Switching lane to LEFT")
                direction = "Left"
            else:
                logging.debug("Switching lane to RIGHT")
                direction = "Right"
            self.msg("switchLane", direction)
            self.switch_command_sent = True
            return True

        if self.track.pieces[piece].is_switch():
            self.switch_command_sent = False
        return False

    def get_car_by_color(self, color):
        if color == self.own_car.color:
            return self.own_car
        for car in self.opponent_cars:
            if car.color == color:
                return car
        return None

    def on_crash(self, data):
        car = self.get_car_by_color(data["color"])
        logging.debug("%s car crashed" % car.color)
        car.crash()
        self.ping()

    def on_game_end(self, data):
        logging.debug("Race ended")
        self.ping()

    def on_error(self, data):
        logging.debug("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        self.color = data["color"]
        logging.debug("Car color: " + self.color)

    def on_game_init(self, data):
        def is_quick_race(data):
            if data["race"]["raceSession"].get("quickRace"):
                logging.debug("race: quick race")
                return True
            return False

        def is_qualifying(data):
            if data["race"]["raceSession"].get("durationMs"):
                logging.debug("race: qualifying")
                return True
            return False

        if is_quick_race(data) or is_qualifying(data):
            self.track = Track(data["race"]["track"])
            car_datas = data["race"]["cars"]
            own_car_data = self.get_own_car_data(car_datas)
            self.own_car = Car(own_car_data, self.track)
            self.opponent_cars = [
                Car(opponent_car_data, self.track, self.own_car.speed_controller)
                for opponent_car_data in self.get_opponent_car_datas(car_datas)
            ]
            self.lane_switcher = LaneSwitcher(self.track)
        else:
            logging.debug("race: competition")
            self.own_car.reset()

        laps = data["race"]["raceSession"].get("laps")
        if laps:
            self.track.laps = laps

        logging.debug("Opponent count: " + str(len(self.opponent_cars)))

    def on_finish(self, data):
        logging.debug("Race finished! Tick: " + str(self.game_tick))
        self.ping()

    def get_own_car_data(self, car_datas):
        return self.get_car_data_with_color(car_datas, self.color)

    def get_opponent_car_datas(self, car_datas):
        return [car_data for car_data in car_datas if not self.is_own_car_data(car_data)]

    def is_own_car_data(self, car_data):
        return car_data["id"]["color"] == self.color

    def get_car_data_with_color(self, car_datas, car_color):
        data = [car for car in car_datas if car["id"]["color"] == car_color]
        if data != []:
            return data[0]
        return None

    def on_turbo_available(self, data):
        self.own_car.update_turbo(data)

    def on_turbo_start(self, data):
        if data["color"] == self.color:
            self.own_car.turbo.on_start()

    def on_turbo_end(self, data):
        if data["color"] == self.color:
            self.own_car.turbo.on_end()

    def on_spawn(self, data):
        if data["color"] == self.color:
            self.own_car.turbo.on_spawn()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'finish': self.on_finish,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'spawn': self.on_spawn,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg_type = None
            try:
                msg = json.loads(line)
                msg_type, data = msg['msgType'], msg['data']
                if "gameTick" in msg:
                    self.game_tick = msg["gameTick"]
                if msg_type in msg_map:
                    msg_map[msg_type](data)
                else:
                    logging.debug("Got {0}".format(msg_type))
                    self.ping()
            except:
                if msg_type == "carPositions":
                    throttle = 0.3
                    logging.debug("Unexpected exception happened, sending constant throttle: %.2f" % throttle)
                    self.throttle(throttle)
                else:
                    logging.debug("Unexpected exception happened, sending ping")
                    self.ping()
            line = socket_file.readline()


def run_bot(bot):
    if "HWO_TRACKNAME" in os.environ:
        track_name = os.environ.get("HWO_TRACKNAME")
        car_count = os.environ.get("HWO_CARCOUNT")
        password = os.environ.get("HWO_PASSWORD")
        bot.run_custom_race(track_name, int(car_count), password)
    else:
        bot.run()


if __name__ == "__main__":
    config_logging()

    if len(sys.argv) != 5:
        logging.debug("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        logging.debug("Connecting with parameters:")
        logging.debug("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = SirvikoppiBot(s, name, key)
        run_bot(bot)
