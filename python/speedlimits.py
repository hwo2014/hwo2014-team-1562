import logging

MAX_SPEED = 100
MIN_SPEED = 5


class SpeedLimits(object):
    def __init__(self):
        self.limits = {}

    def update_lower_limit(self, radius, speed):
        old_limit = MIN_SPEED
        new_limit = speed
        upper_limit = self.get_upper_limit_for_radius_bigger_than(radius)
        if radius in self.limits:
            upper_limit = self.limits[radius][1]
            old_limit = self.limits[radius][0]
            new_limit = min(max(old_limit, speed), upper_limit)
        logging.debug("Lower limit for radius %d: %.3f -> %.3f (upper limit: %.3f)"
                      % (radius, old_limit, new_limit, upper_limit))
        self.limits[radius] = (new_limit, upper_limit)
        bigger_radiuses = [r for r in self.limits.keys() if r >= radius]
        for r in bigger_radiuses:
            if self.limits[r][0] < new_limit:
                self.limits[r] = (new_limit, self.limits[r][1])

    def update_upper_limit(self, radius, speed):
        old_limit = MAX_SPEED
        new_limit = speed
        lower_limit = self.get_lower_limit_for_radius_smaller_than(radius)
        new_lower_limit = lower_limit
        if radius in self.limits:
            lower_limit = self.limits[radius][0]
            old_limit = self.limits[radius][1]
            new_limit = min(old_limit, speed)
            new_lower_limit = min(lower_limit, new_limit)
        logging.debug("Upper limit for radius %d: %.3f -> %.3f (lower limit: %.3f -> %.3f) "
                      % (radius, old_limit, new_limit, lower_limit, new_lower_limit))
        self.limits[radius] = (new_lower_limit, new_limit)
        smaller_radiuses = [r for r in self.limits.keys() if r <= radius]
        for r in smaller_radiuses:
            if self.limits[r][1] > new_limit:
                self.limits[r] = (self.limits[r][0], new_limit)

    def get_lower_limit(self, radius):
        if radius in self.limits:
            return self.limits[radius][0]
        return self.get_lower_limit_for_radius_smaller_than(radius)

    def get_upper_limit(self, radius):
        if radius in self.limits:
            return self.limits[radius][1]
        return self.get_upper_limit_for_radius_bigger_than(radius)

    def get_lower_limit_for_radius_smaller_than(self, radius):
        lower_limit = MIN_SPEED
        smaller_radiuses = [r for r in self.limits.keys() if r <= radius]
        smaller_radiuses.sort()
        if smaller_radiuses != []:
            r = smaller_radiuses[-1]
            lower_limit = self.limits[r][0]
        return lower_limit

    def get_upper_limit_for_radius_bigger_than(self, radius):
        upper_limit = MAX_SPEED
        bigger_radiuses = [r for r in self.limits.keys() if r >= radius]
        bigger_radiuses.sort()
        if bigger_radiuses != []:
            r = bigger_radiuses[0]
            upper_limit = self.limits[r][1]
        return upper_limit
