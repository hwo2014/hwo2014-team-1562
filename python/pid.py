class PID(object):
    def __init__(self, Kp, Ki, Kd, setpoint, min_output, max_output):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.setpoint = setpoint
        self.previous_error = 0
        self.integral = 0
        self.min_output = min_output
        self.max_output = max_output

    def set_kp(self, Kp):
        self.Kp = Kp

    def set_ki(self, Ki):
        self.Ki = Ki

    def set_kd(self, Kd):
        self.Kd = Kd

    def set_setpoint(self, setpoint):
        if abs(self.setpoint - setpoint) > 0.01:
            self.integral = 0
        self.setpoint = setpoint

    def run(self, measurement, time_delta):
        error = self.setpoint - measurement
        self.integral = self.integral + error * time_delta

        if self.integral > self.max_output:
            self.integral = self.max_output
        if self.integral < self.min_output:
            self.integral = self.min_output

        derivative = 0
        if time_delta != 0:
            derivative = (error - self.previous_error) / time_delta

        output = self.Kp * error + self.Ki * self.integral + self.Kd * derivative
        self.previous_error = error

        if output > self.max_output:
            return self.max_output
        if output < self.min_output:
            return self.min_output
        return output
