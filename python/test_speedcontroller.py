import unittest
from speedcontroller import SpeedController
from track import Track


class TestSpeedController(unittest.TestCase):
    def setUp(self):
        self.track_data = {
            "id": "indianapolis",
            "name": "Indianapolis",
            "pieces": [
                {
                    "radius": 100,
                    "angle": 90,
                },
                {
                    "length": 50.0,
                },
                {
                    "radius": 200,
                    "angle": 90,
                },
                {
                    "length": 50.0,
                },
                {
                    "radius": 50,
                    "angle": 90,
                },
            ],
            "lanes": [
                {
                    "distanceFromCenter": -20,
                    "index": 0
                },
                {
                    "distanceFromCenter": 0,
                    "index": 1
                },
                {
                    "distanceFromCenter": 20,
                    "index": 2
                }
            ],
        }
        self.track = Track(self.track_data)
        self.controller = SpeedController(self.track)

    def test_calculate_speed_increase(self):
        self.assertEqual(0.3, self.controller.calculate_speed_increase(0, 90))
        self.assertEqual(0.3, self.controller.calculate_speed_increase(0, 60))
        self.assertEqual(0, self.controller.calculate_speed_increase(55, 60))
        self.assertEqual(0, self.controller.calculate_speed_increase(45, 60))
        self.assertEqual(0.3, self.controller.calculate_speed_increase(0, 40))
        self.assertEqual(0.3, self.controller.calculate_speed_increase(0, 20))
        self.assertEqual(0.1, self.controller.calculate_speed_increase(1, 51))
        self.assertEqual(0, self.controller.calculate_speed_increase(12, 51))


if __name__ == '__main__':
    unittest.main()
