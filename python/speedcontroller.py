import logging
from track import Bend
from speedlimits import SpeedLimits
from pid import PID

MAX_SPEED = 100
CRASH_SPEED_REDUCE = 0.4
PIECE_LOOKAHEAD_COUNT = 3
LOOKAHEAD_SPEED_INCREASE = 6
MAX_ANGLE_WITH_SPEED_INCREASE = 15
DEBUG = False


class SpeedController(object):

    def __init__(self, track):
        self.speedlimits = SpeedLimits()
        self.track = track
        self.pid = PID(1.0, 1.0, 1.0, MAX_SPEED, 0, 1)
        self.throttle_strategy = "speed_control_pid"

        self.throttle_strategy_map = {
            "slip_angle_thermostate_control": self.__slip_angle_thermostate_control,
            "speed_control_pid": self.__speed_control_pid,
            "constant_throttle": self.__constant_throttle,
        }

    def get_target_speed(self, position):
        speed = position.speed
        lookahead_count = PIECE_LOOKAHEAD_COUNT
        if speed is not None and speed > LOOKAHEAD_SPEED_INCREASE:
            lookahead_count += 1
        lane = position.lane
        current_index = position.pieceIndex
        pieces = [self.track.get_nth_piece_after(
            current_index, n) for n in range(0, lookahead_count + 1)]
        bends = [piece for piece in pieces if isinstance(piece, Bend)]
        limits = [self.speedlimits.get_lower_limit(
            bend.get_radius(lane)) for bend in bends]
        limits = [limit for limit in limits if limit is not None]
        if limits == []:
            return MAX_SPEED
        else:
            return min(limits)

    def on_crash(self, speed, position):
        piece = position.get_current_trackpiece()
        if isinstance(piece, Bend):
            radius = piece.get_radius(position.lane)
            self.speedlimits.update_upper_limit(
                radius, speed - CRASH_SPEED_REDUCE)
        else:
            current_index = position.pieceIndex
            previous_piece = self.track.pieces[
                (current_index - 1) % len(self.track.pieces)]
            if isinstance(previous_piece, Bend):
                radius = previous_piece.get_radius(position.lane)
                self.speedlimits.update_upper_limit(
                    radius, speed - CRASH_SPEED_REDUCE)

    def change_throttle_strategy(self, new_strategy):
        self.throttle_strategy = new_strategy

    def update_throttle(self, position):
        new_throttle = self.throttle_strategy_map[self.throttle_strategy](position)
        return new_throttle

    def __slip_angle_thermostate_control(self, position):
        max_slip_angle = 20
        max_throttle = 0.7
        min_throttle = 0
        if (position.angle < max_slip_angle):
            return max_throttle
        else:
            return min_throttle

    def __speed_control_pid(self, position):
        target_speed = self.get_target_speed(position)
        self.pid.set_setpoint(target_speed)
        return self.pid.run(position.speed, position.delta_tick())

    def __constant_throttle(self):
        return 0.5

    def on_track_piece_change(self, previous_piece, previous_index, position, crash_angle):
        max_angle = max(abs(position.angle), previous_piece.get_maximum_absolute_slip_angle())
        if max_angle is not None:
            if DEBUG:
                logging.debug("Previous piece index %d max slip angle was: %2.3f" %
                              (previous_index, max_angle))

        if max_angle < crash_angle - MAX_ANGLE_WITH_SPEED_INCREASE and isinstance(previous_piece, Bend):
            speed_increase = self.calculate_speed_increase(max_angle, crash_angle)
            radius = previous_piece.get_radius(position.lane)
            previous_limit = self.speedlimits.get_lower_limit(radius)
            upper_limit = self.speedlimits.get_upper_limit(radius)
            new_limit = min(upper_limit, previous_limit + speed_increase)
            if position.speed > previous_limit - 0.15:
                # Only increase the speed limit if we were running the limit speed!
                # It is no use increasing the limit if we did not reach that limit
                self.speedlimits.update_lower_limit(radius, new_limit)

    def calculate_speed_increase(self, max_angle, crash_angle):
        if max_angle < 0.01:
            return 0.3
        if max_angle <= max(0, crash_angle - 50):
            return 0.1
        return 0
