LEFT = -1
RIGHT = 1
THRESHOLD_DISTANCE_TO_CAR_FRONT = 100


class LaneSwitcher(object):
    def __init__(self, track):
        self.track = track

    def should_switch_lane_to(self, position, other_car_positions):
        current_lane_index = position.lane
        piece = self.track.pieces[position.pieceIndex]
        if piece.is_switch():
            return None

        lane = self.choose_lane(position, other_car_positions)
        if lane < current_lane_index:
            return LEFT
        elif lane > current_lane_index:
            return RIGHT
        else:
            return None

    def is_correct_piece_to_send_switch_command(self, current_piece_index):
        next_piece_index = (current_piece_index + 1) % len(self.track.pieces)
        return self.track.pieces[next_piece_index].is_switch()

    def choose_lane(self, current_position, other_car_positions):
        current_lane_index = current_position.lane
        possible_lanes = [current_lane_index] + self.track.get_possible_lanes_to_switch(current_lane_index)

        def lane_sorter(index1, index2):
            car_positions_on_lane_1 = [pos for pos in other_car_positions if pos.lane == index1]
            car_positions_on_lane_2 = [pos for pos in other_car_positions if pos.lane == index2]
            pos_cars_in_front_lane_1 = [pos for pos in car_positions_on_lane_1 if current_position.real_distance_to(pos) < THRESHOLD_DISTANCE_TO_CAR_FRONT]
            pos_cars_in_front_lane_2 = [pos for pos in car_positions_on_lane_2 if current_position.real_distance_to(pos) < THRESHOLD_DISTANCE_TO_CAR_FRONT]
            if pos_cars_in_front_lane_1 != [] and pos_cars_in_front_lane_2 == []:
                return 1
            elif pos_cars_in_front_lane_1 == [] and pos_cars_in_front_lane_2 != []:
                return -1
            else:
                d1 = self.lane_distance_between_next_switches(index1, current_position.pieceIndex)
                d2 = self.lane_distance_between_next_switches(index2, current_position.pieceIndex)
                if d1 < d2:
                    return -1
                elif d1 > d2:
                    return 1
                else:
                    if current_position.lane == index1:
                        return -1
                    else:
                        return 1

        possible_lanes.sort(cmp=lane_sorter)
        return possible_lanes[0]

    def lane_distance_between_next_switches(self, lane_index, current_piece_index):
        next_switch = self.track.get_index_of_next_switch_piece(current_piece_index)
        switch_after_next_switch = self.track.get_index_of_next_switch_piece(next_switch)
        return self.track.get_lane_distance_between_pieces(lane_index, next_switch, switch_after_next_switch)
