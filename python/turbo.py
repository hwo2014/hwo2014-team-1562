import logging
class Turbo(object):
    def __init__(self):
        self.available = False
        self.crashed = False
        self.enabled = False

    def set_turbo_available(self, turbo_data):
        if self.crashed:
            logging.debug("Did not get turbo because of crash :-(")
        else:
            self.available = True
            self.durationMillis = turbo_data["turboDurationMilliseconds"]
            self.durationTicks = turbo_data["turboDurationTicks"]
            self.factor = turbo_data["turboFactor"]
            logging.debug("Turbo available. Duration ms: %d, duration ticks: %d, factor: %f" % (self.durationMillis, self.durationTicks, self.factor))

    def is_available(self):
        return self.available

    def should_use_turbo_here(self, track, position):
        index = position.pieceIndex
        lap = position.lap
        return track.on_beginning_of_final_straight(index, lap)

    def on_start(self):
        logging.debug("Turbo started!")
        self.available = False
        self.enabled = True

    def on_end(self):
        logging.debug("Turbo ended!")
        self.enabled = False

    def on_crash(self):
        self.crashed = True
        self.available = False
        self.enabled = False

    def on_spawn(self):
        self.crashed = False

    def is_enabled(self):
        return self.enabled
